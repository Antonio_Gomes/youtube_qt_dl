import sys
import os

tkinter_is_installed: bool = True
try:
    import tkinter as tk
    from tkinter import messagebox
except:
    tkinter_is_installed = False


def install_library(str_library: str, autoclose: bool = True):
    if tkinter_is_installed:
        window = tk.Tk()
        msg = messagebox.askokcancel(title="Bibliothèque manquante",
                                     message="La bibliothèque " + str_library + " n'est pas installée sur votre python, voulez vous la "
                                                                                "télécharger ?")
        if msg:
            if os.name == "posix":
                os.system("python -m pip install " + str_library)
            elif os.name == "nt":
                os.system("pip install " + str_library)
            return True
        else:
            if autoclose:
                window.destroy()
                sys.exit()
            else:
                return False

    else:
        reponse: str = "0"
        while reponse != "y" and reponse != "n":
            reponse = input("La bibliothèque " + str_library + " n'est pas installée sur votre python, "
                                                               "voulez vous la télécharger ? (y/n) : ").lower().strip()
            if reponse == "y":
                if os.name == "posix":
                    os.system("python -m pip install " + str_library)
                elif os.name == "nt":
                    os.system("pip install " + str_library)
                    os.system("pip3 install " + str_library)
                return True
            elif reponse == "n":
                if autoclose:
                    sys.exit()
                else:
                    return False
