import phonenumbers

from phonenumbers import geocoder
from phonenumbers import carrier
from phonenumbers import phonemetadata

numero_tel = input("Mettez le numéro de téléphone : ")
numero_tel = "+33" + numero_tel[1:]
numero_tel = phonenumbers.parse(numero_tel)

print(geocoder.description_for_number(numero_tel, "fr"))
print(carrier.name_for_number(numero_tel, "fr"))
print(phonemetadata.NumberFormat(numero_tel, "fr"))