import PySide6.QtWidgets as qtw
from PySide6.QtUiTools import QUiLoader
from PySide6 import QtCore
from PySide6.QtCore import QObject, QUrl, QThread
from PySide6.QtGui import QStandardItemModel, QStandardItem
import xml.etree.cElementTree as etree


class GUI_options(QObject):
    def __init__(self, parent=None):
        super(GUI_options, self).__init__(parent)
        self.param = etree.parse("youtube_options.xml")

        loader = QUiLoader()
        self.fenetre = loader.load("GUI_QT_options.ui", None)
        self.fenetre.setWindowTitle("Options (Youtube_DL)")
        self.bbox_valider: qtw.QDialogButtonBox = self.fenetre.findChild(qtw.QDialogButtonBox, "bbox_valider")
        self.cb_autoc_name: qtw.QCheckBox = self.fenetre.findChild(qtw.QCheckBox, "cb_autocomplete_name")
        self.cb_autoc_name.setChecked((self.param.find("autocomplete_name").text == "True"))

        self.cb_autoc_name.stateChanged.connect(self.param_autocomplete_name)
        self.bbox_valider.accepted.connect(self.valider)

    def param_autocomplete_name(self):
        if self.cb_autoc_name.isChecked():
            self.param.find("autocomplete_name").text = "True"
        else:
            self.param.find("autocomplete_name").text = "False"

    def show(self):
        self.fenetre.show()

    def valider(self):
        self.param.write("youtube_options.xml")
