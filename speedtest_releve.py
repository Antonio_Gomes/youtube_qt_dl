from time import sleep
import datetime
import tkinter as tk
import tkinter.ttk as ttk
import speedtest


def test_debit():
    servers = []
    threads = 2
    st = speedtest.Speedtest()
    st.get_servers(servers)
    st.get_best_server()
    date = datetime.datetime.now()
    ecrire("----------", nom_fichier)
    ecrire(str(date.hour) + ":" + str(date.minute) + ":" + str(date.second), nom_fichier)
    download = round(st.download(threads=threads) / 8000000, 2)
    ecrire("Téléchargement : " + str(download) + "Mo/s", nom_fichier)
    upload = round(st.upload(threads=threads) / 800000, 2)
    ecrire("Téléversement : " + str(upload) + "Mo/s", nom_fichier)


def ecrire(message: str, nom_fichier: str = None, console_tkinter: tk.Text = None, saut_de_ligne: bool = True):
    if saut_de_ligne:
        message = message + "\n"
    print(message)
    if nom_fichier:
        fichier = open(nom_fichier, "a")
        fichier.write(message)
        fichier.close()
    if console_tkinter:
        console_tkinter.insert("end", message)
        console_tkinter.see("end")


nom_fichier = "relevé_debit.txt"
fichier = open(nom_fichier, "a")
fichier.write("-----------------------------------------------\n")
date = (str(datetime.datetime.now()).split("."))[0]
fichier.write("Nouvelle analyse : " + date + "\n")
fichier.close()
test_debit()
test_debit()
