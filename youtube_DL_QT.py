import sys
import os
from _thread import start_new_thread
from threading import Thread
from typing import Tuple, List
import xml.etree.cElementTree as etree
import autoinstall_library as autolib

try:
    import PySide6.QtWidgets as qtw
    from PySide6.QtUiTools import QUiLoader
    from PySide6 import QtCore
    from PySide6.QtCore import QObject, QUrl, QThread
    from PySide6.QtGui import QStandardItemModel, QStandardItem
except ModuleNotFoundError:
    autolib.install_library("PySide6")
except ImportError:
    # Cas spécial sur linux sur une librairie manquante
    if os.name == "posix":
        os.system("sudo apt install libopengl0 -y")

try:
    import youtube_dl.YoutubeDL
except ModuleNotFoundError:
    autolib.install_library("youtube_dl")

if os.name == "posix":
    os.system("python -m pip install --upgrade youtube-dl")
elif os.name == "nt":
    os.system("pip install --upgrade youtube-dl")

from GUI_options import GUI_options

def percent_str_to_int(percent_str: str):
    percent_str = percent_str.replace("%", "")
    percent_str = percent_str.strip()
    return float(percent_str)


class GUI_youtube(QObject):
    # noinspection PyTypeChecker
    def __init__(self, ui_file="GUI_QT_Youtube_DL.ui", parent=None):
        super(GUI_youtube, self).__init__(parent)
        self.param = etree.parse("youtube_options.xml")

        loader = QUiLoader()
        self.fenetre = loader.load(ui_file, None)
        self.e_URL: qtw.QLineEdit = self.fenetre.findChild(qtw.QLineEdit, "e_URL")
        self.e_URL.textChanged.connect(self.autocomplete_name)
        self.e_name: qtw.QLineEdit = self.fenetre.findChild(qtw.QLineEdit, "e_name")
        self.pbar: qtw.QProgressBar = self.fenetre.findChild(qtw.QProgressBar, "progress_bar")
        self.c_choix: qtw.QComboBox = self.fenetre.findChild(qtw.QComboBox, "c_choix")
        self.c_choix.addItems([".mp3", ".mp4", ".m4a"])
        self.b_reset_URL: qtw.QPushButton = self.fenetre.findChild(qtw.QPushButton, "b_reset_URL")
        self.b_reset_URL.clicked.connect(self.reset_e_URL)
        self.b_reset_name: qtw.QPushButton = self.fenetre.findChild(qtw.QPushButton, "b_reset_name")
        self.b_reset_name.clicked.connect(self.reset_e_name)
        self.b_valider: qtw.QPushButton = self.fenetre.findChild(qtw.QPushButton, "b_valider")
        self.b_valider.clicked.connect(self.valider)
        self.b_ajouter: qtw.QPushButton = self.fenetre.findChild(qtw.QPushButton, "b_ajouter")
        self.b_ajouter.clicked.connect(self.ajouter_video)
        self.b_research_name: qtw.QPushButton = self.fenetre.findChild(qtw.QPushButton, "b_research_name")
        self.b_research_name.clicked.connect(self.research_name)
        self.text_console: qtw.QTextBrowser = self.fenetre.findChild(qtw.QTextBrowser, "text_console")
        self.l_liste_videos: qtw.QListView = self.fenetre.findChild(qtw.QListView, "l_liste_videos")
        self.l_liste_videos.setModel(element_liste_video.liste_des_noms)
        self.desc_nom_video: qtw.QLabel = self.fenetre.findChild(qtw.QLabel, "desc_nom_video")
        self.b_option: qtw.QPushButton = self.fenetre.findChild(qtw.QPushButton, "b_option")
        self.b_option.clicked.connect(self.change_option)

        self.fenetre.show()

    def reset_e_URL(self):
        self.e_URL.clear()

    def reset_e_name(self):
        self.e_name.clear()

    def ecrire(self, message):
        print(message)
        self.text_console.append(message)

    def change_option(self):
        fen_option = GUI_options(self)
        fen_option.show()

    def valider(self):
        self.ajouter_video()
        self.ecrire("-------------------")
        # start_new_thread(self.telecharger_video, ())
        self.telecharger_video()

    def research_name(self):
        youtube_url: str = self.e_URL.text()
        if youtube_url != "":
            if "list" in youtube_url and "youtu" in youtube_url:
                youtube_url = youtube_url.split("list")[0]
            ydl_opts = {}
            try:
                info_dict = youtube_dl.YoutubeDL(ydl_opts).extract_info(youtube_url, download=False)
                self.e_name.setText(info_dict['title'])
            except youtube_dl.utils.DownloadError:
                self.ecrire("Accès impossible : vérifiez que l'URL donné est valide")
                self.b_valider.setEnabled(True)
                return

    def autocomplete_name(self):
        if self.param.find("autocomplete_name").text == "True":
            self.research_name()

    def ajouter_video(self):
        youtube_url: str = self.e_URL.text()
        if youtube_url != "":
            if "list" in youtube_url and "youtu" in youtube_url:
                youtube_url = youtube_url.split("list")[0]
            ydl_opts = {}
            try:
                info_dict = youtube_dl.YoutubeDL(ydl_opts).extract_info(youtube_url, download=False)
            except youtube_dl.utils.DownloadError:
                self.ecrire("Accès impossible : vérifiez que l'URL donné est valide")
                self.b_valider.setEnabled(True)  # Pas utile
                return
            if self.e_name.text() == "":
                song_title: str = info_dict['title']
            else:
                song_title: str = self.e_name.text()
            # Permet d'avoir un nom de fichier correct sur Windows
            trans_table = str.maketrans(
                {"/": "", "\\": "", "\"": "", ":": "", "*": "", "?": "", "|": "", "<": "", ">": "", "#": ""})
            song_title = song_title.translate(trans_table)
            song_title = song_title.strip()
            choix = self.c_choix.currentText()
            video = element_liste_video(youtube_url, song_title, choix)
            self.e_URL.clear()
            self.e_name.clear()
            # self.l_liste_videos.addItems(element_liste_video.liste_des_noms)
            self.ecrire("Le lien est valide, " + song_title + " est sur liste d'attente.")

    def progress_bar(self, d):
        if d['status'] == 'downloading':
            try:
                self.pbar.setValue(percent_str_to_int(d['_percent_str']))
            except:
                pass
        elif d['status'] == 'finished':
            self.ecrire("Téléchargement terminé, conversion en cours...")
            self.pbar.setValue(0)

    def telecharger_video(self):
        # Fonction pour télécharger les vidéos
        dir_music: Tuple[QUrl] = qtw.QFileDialog.getExistingDirectory(self.fenetre,
                                                                      "Sélectionnez le lieu de l'enregistrement")
        # dir_music = dir_music[0].toLocalFile()
        print(dir_music)
        ligne: int = -1  # Pour que ça commence avec ligne = 0 dans la boucle
        if dir_music == "":
            self.b_valider.setEnabled(True)
            return
        for video in element_liste_video.liste_des_elements:
            ligne += 1
            print(element_liste_video.liste_des_elements)
            self.b_valider.setEnabled(False)
            self.desc_nom_video.setText(video.nom_video)
            youtube_url = video.lien_video
            song_title = video.nom_video
            ydl_opts = {}
            outtmpl = dir_music + "/" + song_title + '.%(ext)s'
            choix = video.extension
            if choix == ".mp3":
                ydl_opts = {
                    'format': 'bestaudio/best',
                    'noplaylist': True,
                    'outtmpl': outtmpl,
                    'progress_hooks': [self.progress_bar],
                    'postprocessors': [
                        {'key': 'FFmpegExtractAudio', 'preferredcodec': 'mp3',
                         'preferredquality': '192',
                         },
                        {'key': 'FFmpegMetadata'},
                    ],
                }
            elif choix == ".mp4":
                ydl_opts = {
                    'format': 'best',
                    'progress_hooks': [self.progress_bar],
                    'outtmpl': outtmpl,
                    'noplaylist': True,
                }
            elif choix == ".m4a":
                ydl_opts = {
                    'format': 'bestaudio/best',
                    'outtmpl': outtmpl,
                    'noplaylist': True,
                    'progress_hooks': [self.progress_bar],
                    'postprocessors': [
                        {'key': 'FFmpegExtractAudio', 'preferredcodec': 'm4a',
                         },
                        {'key': 'FFmpegMetadata'},
                    ],
                }
            elif choix == ".wav":
                ydl_opts = {
                    'format': 'bestaudio/best',
                    'outtmpl': outtmpl,
                    'noplaylist': True,
                    'progess_hooks': [self.progress_bar],
                    'postprocessors': [
                        {'key': 'FFmpegExtractAudio', 'preferredcodec': 'wav',
                         },
                        {'key': 'FFmpegMetadata'},
                    ],
                }
            try:
                self.ecrire("Vidéo (" + song_title + ") trouvée, téléchargement en cours...")
                video_song = youtube_dl.YoutubeDL(ydl_opts).download([youtube_url])
            except:
                self.ecrire(
                    "La bibliothèque ffmpeg n'est pas installé sur le système, conversion alternative en cours... (attendre une minute après fin de conversion)")
                try:
                    os.rename(dir_music + "/" + song_title + ".webm", dir_music + "/" + song_title + choix)
                except:
                    os.rename(dir_music + "/" + song_title + ".m4a", dir_music + "/" + song_title + choix)
            self.ecrire("Conversion terminée")
            self.ecrire(song_title + choix + " a été enregistré sur " + dir_music)
            self.ecrire("")
            element_liste_video.enlever_premier_nom(self, ligne)
            # Thread(target=element_liste_video.enlever_premier_nom).start()
            self.b_valider.setEnabled(True)
        # gui.affichage_liste.configure(values=element_liste_video.liste_des_noms)
        element_liste_video.reset_liste()
        self.desc_nom_video.setText("Pas de vidéo")


class element_liste_video:
    liste_des_noms = QStandardItemModel()
    liste_des_elements = []

    def __init__(self, lien_video: str, nom_video: str, extension: str):
        self.lien_video = lien_video
        self.nom_video = nom_video
        element_liste_video.liste_des_noms.appendRow(QStandardItem(self.nom_video))
        self.extension = extension
        element_liste_video.liste_des_elements.append(self)

    @staticmethod
    def enlever_premier_nom(GUI: GUI_youtube, ligne: int):
        # element_liste_video.liste_des_noms.removeRow(0)
        GUI.l_liste_videos.setRowHidden(ligne, True)
        # GUI.l_liste_videos.model().removeRow(0)
        # GUI.l_liste_videos.setModel(element_liste_video.liste_des_noms)

    @staticmethod
    def reset_liste():
        element_liste_video.liste_des_noms.clear()
        element_liste_video.liste_des_elements = []
        # element_liste_video.liste_des_noms = QStandardItemModel()
        # element_liste_video.liste_des_elements = []




if __name__ == "__main__":
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
    app = qtw.QApplication(sys.argv)
    form = GUI_youtube()
    sys.exit(app.exec_())
